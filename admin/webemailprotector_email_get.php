<?php
/**
 * @file
 * Change the email address in the db.
 */

/**
 * Implements Email Get function.
 */
function webemailprotector_email_get($i) {
  $email = variable_get('wepdb_wep_email_' . $i);
  echo $email;
}

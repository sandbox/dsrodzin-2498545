<?php
/**
 * @file
 * Change display name in the db.
 */

/**
 * Implements Display Name Change function.
 */
function webemailprotector_displayname_change($i, $displayname) {
  variable_set('wepdb_wep_display_name_' . $i, $displayname);
  echo $displayname;
}

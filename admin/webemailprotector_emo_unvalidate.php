<?php
/**
 * @file
 * Remove the emo from the db.
 */

/**
 * Implements EMO unvalidate function.
 */
function webemailprotector_emo_unvalidate($i) {
  variable_set('wepdb_wep_validated_' . $i, 'FALSE');
  variable_set('wepdb_wep_emo_' . $i, 'xxxx-xxxx-xxxx-xxxx-xxxx');
  $arr = array(
    'emo_nu' => $i,
  );
  echo json_encode($arr);
}

/**
 * @file
 * Javascript to make the setting page run.
 */

/**
 * Comment function.
 */
function webemailprotector_emo_init($current_user_email, $wep_ver, $wep_reason) {
  jQuery.ajax({
    url: 'http://www.webemailprotector.com/cgi-bin/cms/emo_init.py',
    type: "GET",
    crossDomain: true,
    data: {'adminemail':$current_user_email, 'wep_ver':$wep_ver, 'wep_reason':$wep_reason, 'cms':'dr'},
    dataType: "jsonp",
    cache: false});
}
/**
 * Comment function.
 */
function webemailprotector_emo_init_cb(response) {
  document.getElementById('wep_spinner').style.display = 'none';
  if (response.success == "true") {
    alert(response.message);
  }
  if (response.success == "false") {
    alert(response.message);
  }
  document.getElementById('wep_dullout').style.display = 'none';
}
/**
 * Comment function.
 */
function webemailprotector_emo_delete($emo_nu) {
  jQuery.ajax({
    type: "POST",
    dataType: 'json',
    url: "webemailprotector_emo_delete_callback/" + $emo_nu,
    success: function(response) {
      alert("email no." + response.emo_nu + " deleted, " + response.nuemails + " remaining");
      // Delete the old row from the display.
      rowID = 'wep_tablerow_' + response.emo_nu;
      document.getElementById(rowID).parentNode.removeChild(document.getElementById(rowID));
    }
  });
}
/**
 * Comment function.
 */
function webemailprotector_emo_new() {
  jQuery.ajax({
    type: "POST",
    dataType: 'json',
    url: "webemailprotector_emo_new_callback",
    success: function (response) {
        tableID = 'wep_table';
        var row = document.getElementById(tableID).insertRow(response.row);
        row.id = 'wep_tablerow_' + response.id;
        var openbrackettxt = row.insertCell(0);
        openbrackettxt.outerHTML = "<td style=\"font-size:30px;padding-bottom:10px;\">[</td>";
        var emailtxt = row.insertCell(1);
        emailtxt.innerHTML = "<input type=\"text\" id=\"wep_emailtxt_" + response.id + "\" value=\"your email address " + response.id + "\" style=\"color:red;\" onkeyup=\"webemailprotector_email_change('" + response.id + "',this.value)\">";
        var closebrackettxt = row.insertCell(2);
        closebrackettxt.outerHTML = "<td style=\"font-size:30px;padding-bottom:10px;\">]</td>";
        var displaytxt = row.insertCell(3);
        displaytxt.innerHTML = "<input type=\"text\" id=\"wep_displaytxt_" + response.id + "\" value=\"your web text " + response.id + "\" onkeyup=\"webemailprotector_displayname_change('" + response.id + "',this.value)\">";
        var registerkey = row.insertCell(4);
        registerkey.innerHTML = "<a id=\"wep_register_" + response.id + "\" class=\"button add another\" onclick=\"webemailprotector_register('" + response.id + "')\" >REGISTER</a>";
        var validatekey = row.insertCell(5);
        validatekey.innerHTML = "<input id=\"wep_validate_" + response.id + "\" type=\"button\" class=\"button add another\" value=\"VALIDATE\" onclick=\"webemailprotector_validate('" + response.id + "','" + response.current_user_email + "')\">";
        var deletekey = row.insertCell(6);
        deletekey.innerHTML = "<input id=\"wep_delete_" + response.id + "\" type=\"button\" class=\"button add another\" value=\"DELETE\" onclick=\"webemailprotector_emo_delete('" + response.id + "')\">";
        textfieldID = 'wep_emailtxt_' + response.id;
        document.getElementById(textfieldID).style.color = "red";
    }
  });
}
/**
 * Comment function.
 */
function webemailprotector_email_change($emo_nu, $email) {
  jQuery.ajax({
    type: "POST",
    url: "webemailprotector_email_change_callback/" + $emo_nu + "/" + $email,
    success: function (response) {
    }
  });
  textfieldID = 'wep_emailtxt_' + $emo_nu;
  document.getElementById(textfieldID).style.color = "red";
}
/**
 * Comment function.
 */
function webemailprotector_displayname_change($emo_nu,$displayname) {
  $displayname = $displayname.replace("'", "");
  $displayname = $displayname.replace('"', '');
  textfieldID = 'wep_displaytxt_' + $emo_nu;
  document.getElementById(textfieldID).value = $displayname;
  jQuery.ajax({
    type: "POST",
    url: "webemailprotector_displayname_change_callback/" + $emo_nu + "/" + $displayname,
    success: function (response) {
    }
  });
}
/**
 * Comment function.
 */
function webemailprotector_donothing() {
}
/**
 * Comment function.
 */
function webemailprotector_register($emo_nu) {
  jQuery.ajax({
    type: "POST",
    url: "webemailprotector_register_callback/" + $emo_nu,
    success: function (response) {
      window.open('http://www.webemailprotector.com/cgi-bin/reg.py?cms=dr&email=' + response);
    }
  });
}
/**
 * Comment function.
 */
function webemailprotector_validate($emo_nu, $current_user_email) {
  // Start spinner.
  document.getElementById('wep_spinner').style.display = 'block';
  document.getElementById('wep_dullout').style.display = 'block';
  setTimeout('webemailprotector_donothing()',1000);
  // To make sure any update to the email address has reached the db.
  email = 'undefined';
  // First get the email address from db associated with emo_nu as may have been updated since last php load.
  jQuery.ajax({
    type: "GET",
    url: "webemailprotector_email_get_callback/" + $emo_nu,
    // Then if successful interrogate the server.
    success: function (response) {
      email = response;
      // Jsonp as cross domain.
      jQuery.ajax({
        url: 'http://www.webemailprotector.com/cgi-bin/cms/emo_validate.py',
        type: "GET",
        crossDomain: true,
        data: {'email':email, 'emo_nu':$emo_nu, 'current_user_email':$current_user_email, 'cms':'dr'},
        dataType: "jsonp",
        cache: false });
      }
     });
}
/**
 * Comment function.
 */
function webemailprotector_emocb(response) {
  document.getElementById('wep_spinner').style.display = 'none';
  if (response.success == "true") {
    alert(response.message);
    $code1 = response.code_1;
    $code2 = response.code_2;
    $code3 = response.code_3;
    $code4 = response.code_4;
    $code5 = response.code_5;
    $emo_nu = response.emo_nu;
    $email = response.email;
    // Update the valid status for that element in db with another ajax call.
    jQuery.ajax({
      type: "GET",
      dataType: 'json',
      url: "webemailprotector_emo_validate_callback/" + $code1 + "/" + $code2 + "/" + $code3 + "/" + $code4 + "/" + $code5 + "/" + $emo_nu + "/" + $email,
        success: function (next_response) {
          textfieldID = 'wep_emailtxt_' + next_response.emo_nu;
          document.getElementById(textfieldID).style.color = "green";
        }
    });
  }
  if (response.success == "false") {
    alert(response.message);
    // Update the unvalid status in the db with another ajax.
    jQuery.ajax({
      type: "GET",
      dataType: 'json',
      url: "webemailprotector_emo_unvalidate_callback/" + response.emo_nu,
      success: function (next_response) {
        textfieldID = 'wep_emailtxt_' + response.emo_nu;
        document.getElementById(textfieldID).style.color = "red";
      }
    });
  }
  document.getElementById('wep_dullout').style.display = 'none';
}
/**
 * Comment function.
 */
function webemailprotector_emo_act($current_user_email) {
  jQuery.ajax({
    url: 'http://www.webemailprotector.com/cgi-bin/cs/emo_act.py',
    type: "POST",
    crossDomain: true,
    data: {'current_user_email':$current_user_email,'cms':'dr'},
    dataType: "jsonp",
    cache: false });
}
